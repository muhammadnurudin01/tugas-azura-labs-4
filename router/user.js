const express = require("express");
const UserController = require("../controllers/user.controller");
// creates a new router instance.
const router = express.Router();

router.post("/register", UserController.register);
router.post("/login", UserController.login);

module.exports = router;
