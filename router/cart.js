const express = require("express");
const CartController = require("../controllers/cart.controller");
const router = express.Router();
const { verifyToken, allowedAdmin, allowedUser } = require("../helpers");

router.post("/addcart", [verifyToken, allowedAdmin], CartController.addCart);
router.get("/all", [(verifyToken, allowedAdmin)], CartController.allCart);
router.get(
	"/singlecart/:id",
	[verifyToken, allowedAdmin],
	CartController.singlecart
);
router.patch(
	"/editcart/:id",
	[verifyToken, allowedAdmin],
	CartController.editCart
);
router.delete(
	"/deletecart/:id",
	[verifyToken, allowedAdmin],
	CartController.deletecart
);

module.exports = router;
