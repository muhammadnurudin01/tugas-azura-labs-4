let userModel = require("../models").user;
let bcrypt = require("bcrypt");
const { generateToken, verifyToken } = require("../helpers");

class UserController {
	static async register(req, res) {
		try {
			req.body.password = bcrypt.hashSync(req.body.password, 10);
			const data = await userModel.create(req.body);
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
	static async login(req, res) {
		try {
			const data = await userModel.findOne({
				where: {
					username: req.body.username,
				},
			});
			if (data) {
				let hash = bcrypt.compareSync(req.body.password, data.password);
				if (hash) {
					let users = generateToken(data);
					res.send(users);
				} else {
					res.send("incorrect password");
				}
			} else {
				res.send("Null");
			}
		} catch (error) {
			res.send(error);
		}
	}
}
module.exports = UserController;
