let productModel = require("../models").product;
let categoryModel = require("../models").category;
let cartModel = require("../models").cart;
let cartdetailModel = require("../models").cart_detail;
let { dataToken } = require("../helpers");

class CartController {
	static async editCart(req, res) {
		try {
			let { data } = dataToken(req, res);
			let cart = {
				delivery_method: req.body.delivery_method,
				note: req.body.note,
			};
			const cartData = await cartModel.update(cart, {
				where: { id: req.params.id },
			});
			let total_cost = 0;
			let err = 0;

			for (let i = 0; i < req.body.product.length; i++) {
				let exist = await cartdetailModel.findOne({
					where: {
						cartId: req.params.id,
						productId: req.body.product[i].productId,
					},
				});
				let product = await productModel.findOne({
					where: { id: req.body.product[i].productId },
				});
				let details_carts = {
					quantity: req.body.product[i].quantity,
					price: req.body.product[i].quantity * product.price,
					productId: req.body.product[i].productId,
				};
				total_cost = total_cost + details_carts.price;
				if (exist) {
					try {
						let stock = product.stock + exist.quantity;
						console.log(
							product.product_name,
							exist.quantity,
							product.stock,
							req.body.product[i].quantity
						);
						if (stock >= req.body.product[i].quantity) {
							const cart_detail = await cartdetailModel.update(details_carts, {
								where: {
									cartId: req.params.id,
									productId: req.body.product[i].productId,
								},
							});
							const price = await cartModel.update(
								{ total_cost: total_cost, total_price: total_cost },
								{
									where: { id: req.params.id },
								}
							);
							product.stock = stock - req.body.product[i].quantity;
							let new_stock = await product.save();
						} else {
							err = 1;
							res.send(`stock ${product.product_name} kurang`);
							break;
						}
					} catch (error) {
						res.send(error.message);
						console.log(error.message);
					}
				} else {
					if (product.stock - details_carts.quantity >= 0) {
						details_carts.cartId = req.params.id;
						const cart_detail = await cartdetailModel.create(details_carts);
						const price = await cartModel.update(
							{ total_cost: total_cost, total_price: total_cost },
							{
								where: { id: req.params.id },
							}
						);
						product.stock = product.stock - details_carts.quantity;
						let new_stock = await product.save();
					} else {
						err = 1;
						res.send(`stock ${product.product_name} kurang`);
						break;
					}
				}
			}
			if (err === 0) {
				res.status(201).send("Data berhasil diupdate");
			}
		} catch (error) {
			res.send(error.message);
		}
	}
	static async addCart(req, res) {
		try {
			let { data } = dataToken(req, res);
			console.log(data);
			let cart = {
				delivery_method: req.body.delivery_method,
				note: req.body.note,
				userId: data.id,
			};
			const cartData = await cartModel.create(cart);
			let total_cost = 0;
			let detail_product = [];

			for (let i = 0; i < req.body.product.length; i++) {
				let product = await productModel.findOne({
					where: { id: req.body.product[i].productId },
				});
				let details_carts = {
					quantity: req.body.product[i].quantity,
					price: req.body.product[i].quantity * product.price,
					cartId: cartData.id,
					productId: req.body.product[i].productId,
				};
				total_cost = total_cost + details_carts.price;
				if (product.stock - details_carts.quantity >= 0) {
					detail_product.push(details_carts);
				} else {
					detail_product = [];
					res.send(`stock ${product.product_name} kurang`);
					break;
				}
			}
			if (detail_product.length > 0) {
				for (let i = 0; i < detail_product.length; i++) {
					let product = await productModel.findOne({
						where: { id: req.body.product[i].productId },
					});
					const cart_detail = await cartdetailModel.create(detail_product[i]);
					product.stock = product.stock - detail_product[i].quantity;
					let stock = await product.save();
				}
				cartData.total_cost = total_cost;
				cartData.total_price = total_cost;
				let data_cart = await cartData.save();

				res.status(201).send(data_cart);
			}
		} catch (error) {
			res.send(error.message);
		}
	}
	static async allCart(req, res) {
		try {
			const data = await cartModel.findAll({
				include: [{ model: cartdetailModel }],
			});
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
	static async deletecart(req, res) {
		try {
			console.log(req.params.id);
			const cart = await cartModel.destroy({ where: { id: req.params.id } });
			const details = await cartdetailModel.destroy({
				where: { cartId: req.params.id },
			});
			res.send("Data Berhasil Dihapus");
		} catch (error) {
			res.send(error);
		}
	}
	static async singlecart(req, res) {
		try {
			const data = await cartModel.findOne({
				include: [{ model: cartdetailModel }],
				where: { id: req.params.id },
			});
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
	static async getall(req, res) {
		try {
			const data = await productModel.findAll({
				include: [{ model: categoryModel }],
			});
			res.status(201).send(data);
		} catch (error) {
			res.send(error);
		}
	}
}
module.exports = CartController;
