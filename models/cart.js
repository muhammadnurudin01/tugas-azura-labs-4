"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class cart extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			cart.hasMany(models.cart_detail);
			cart.belongsTo(models.user);
			// define association here
		}
	}
	cart.init(
		{
			delivery_method: DataTypes.STRING,
			total_price: DataTypes.INTEGER,
			note: DataTypes.STRING,
			total_cost: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "cart",
		}
	);
	return cart;
};
