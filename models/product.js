"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class product extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			product.hasMany(models.cart_detail);
			product.belongsTo(models.category);
			product.belongsTo(models.seller);
			// define association here
		}
	}
	product.init(
		{
			product_name: DataTypes.STRING,
			price: DataTypes.INTEGER,
			stock: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "product",
		}
	);
	return product;
};
