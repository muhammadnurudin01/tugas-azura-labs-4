"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/**
		 * Add seed commands here.
		 *
		 * Example:
		 * await queryInterface.bulkInsert('People', [{
		 *   name: 'John Doe',
		 *   isBetaMember: false
		 * }], {});
		 */
		await queryInterface.bulkInsert(
			"products",
			[
				{
					product_name: "kopi susu",
					price: 6000,
					sellerId: 1,
					categoryId: 1,
					stock: 5,
				},
				{
					product_name: "susu coklat",
					price: 6000,
					sellerId: 2,
					categoryId: 1,
					stock: 5,
				},
				{
					product_name: "snack",
					price: 3000,
					sellerId: 2,
					categoryId: 2,
					stock: 6,
				},
				{
					product_name: "shampoo",
					price: 3000,
					sellerId: 3,
					categoryId: 3,
					stock: 3,
				},
			],
			{}
		);
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */
	},
};
