"use strict";
module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("cart_details", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			quantity: {
				type: Sequelize.INTEGER,
			},
			price: {
				type: Sequelize.INTEGER,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
				defaultValue: Sequelize.fn("NOW"),
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
				defaultValue: Sequelize.fn("NOW"),
			},
		});
		await queryInterface.addColumn("cart_details", "cartId", {
			type: Sequelize.INTEGER,
			references: {
				model: "carts",
				key: "id",
			},
			onUpdate: "CASCADE",
			onDelete: "CASCADE",
		});
		await queryInterface.addColumn("cart_details", "productId", {
			type: Sequelize.INTEGER,
			references: {
				model: "products",
				key: "id",
			},
			onUpdate: "CASCADE",
			onDelete: "CASCADE",
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("cart_details");
	},
};
