"use strict";
module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("products", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			product_name: {
				type: Sequelize.STRING,
			},
			price: {
				type: Sequelize.INTEGER,
			},
			stock: {
				type: Sequelize.INTEGER,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
				defaultValue: Sequelize.fn("NOW"),
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
				defaultValue: Sequelize.fn("NOW"),
			},
		});
		await queryInterface.addColumn("products", "categoryId", {
			type: Sequelize.INTEGER,
			references: {
				model: "categories",
				key: "id",
			},
			onUpdate: "CASCADE",
			onDelete: "CASCADE",
		});
		await queryInterface.addColumn("products", "sellerId", {
			type: Sequelize.INTEGER,
			references: {
				model: "sellers",
				key: "id",
			},
			onUpdate: "CASCADE",
			onDelete: "CASCADE",
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("products");
	},
};
