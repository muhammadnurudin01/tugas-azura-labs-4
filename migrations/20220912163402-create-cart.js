"use strict";
module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("carts", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			delivery_method: {
				type: Sequelize.STRING,
			},
			total_price: {
				type: Sequelize.INTEGER,
			},
			note: {
				type: Sequelize.STRING,
			},
			total_cost: {
				type: Sequelize.INTEGER,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
				defaultValue: Sequelize.fn("NOW"),
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
				defaultValue: Sequelize.fn("NOW"),
			},
		});
		await queryInterface.addColumn("carts", "userId", {
			type: Sequelize.INTEGER,
			references: {
				model: "users",
				key: "id",
			},
			onUpdate: "CASCADE",
			onDelete: "CASCADE",
		});
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("carts");
	},
};
